package com.automationanywhere.commands.tests;

import com.automationanywhere.botcommand.data.impl.StringValue;
import com.automationanywhere.commands.basic.SimpleAction;
import com.automationanywhere.commands.basic.SimpleAction4Rest;

import static org.testng.Assert.assertEquals;

public class MiscTests {

    public static void main(String[] args){
        //test 1
        SimpleAction command = new SimpleAction();
        StringValue d = command.action("bren");
        assertEquals("Oh hi there, bren",d.toString());

        // test 2
        SimpleAction4Rest command4 = new SimpleAction4Rest();
        StringValue d4 = command4.action("http://localhost:11051/languages");
        System.out.println(d4.toString());
    }
}
